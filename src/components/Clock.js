import React, { Component } from 'react'

import styled from 'styled-components'



class Clock extends Component {

    constructor() {
        super()

        this.state = {
            date: new Date(),
            monthName: ["January", "February", "March", "April", 
            "May", "June", "July" , "August", "September", "October", 
            "November", "December"]
        }
        
    }

    componentDidMount() {
      setInterval(() => this.tick() , 1000)
    }

    componentWillMount() {
      clearInterval(() => this.tick())               
    }

    tick = () => {
        this.setState({
            date: new Date()
        })
    }

    render() {
        const { date, monthName } = this.state 

        

        return (<Wrap>        
            <h2> {date.getDate()} {monthName[date.getMonth()]} {date.getFullYear()} {date.toLocaleTimeString()}.</h2>
        </Wrap>)
    }
}

const Wrap = styled.div`
h2{
position: absolute;
top: 50px;
right: 100px;
color: white;
font-size: 20px;
z-index: 1;
}

`



export default Clock