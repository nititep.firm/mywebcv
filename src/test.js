// const compose = function(f, g) {
//     return function(x) {
//       return f(g(x))
//     }
//   }

// function sanitize(str) {
//     return str.trim().toLowerCase()
//   }

// console.log(sanitize('  Hello My Name is Ham     '))

const trim = s => s.trim()
const toLowerCase = s => s.toLowerCase()

function sanitize(str) {
    return toLowerCase(trim(str))
}




