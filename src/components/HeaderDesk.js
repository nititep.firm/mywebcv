import React from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu, Icon } from 'antd';
import styled from 'styled-components'


const { Header ,Content, Footer, Sider } = Layout;

const HeaderDesk = ({firstName, lastName}) => {
    return <Wrapper>
        <div className="headRow">
        <div className="rowNav">
          <h1 className="title"><Link style={{color: '#72e3ff'}} to="/">Nititep Chareonbamrung</Link></h1>
          <h3 className="sub">Student , Creative, Programmer, Developer</h3>
        </div>
        <div className="rowNav">
            <Menu className="navbar"           
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}>
            <Menu.Item key="1"><Icon type="home" /> Home</Menu.Item>
            <Menu.Item key="2"><Icon type="user" /> About Me</Menu.Item>
            <Menu.Item key="3"><Icon type="edit" /> Skill</Menu.Item>             
            <Menu.Item key="4"><Icon type="star-o" /> Experience</Menu.Item>
            <Menu.Item key="5"><Link to="/port" style={{color: '#72e3ff'}}><Icon type="star-o"/>Portfolio</Link></Menu.Item>
            <Menu.Item key="6"><Icon type="mail" /> Contact</Menu.Item>
          </Menu>
          </div>
        </div>  
    </Wrapper>
}

const Wrapper = styled.div`
background-color: #141414;

.headRow {    
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }
  
  .rowNav {     
  }
  
  .title {
    margin-top: 100px;
    margin-left: 50px;
    color: #72e3ff;
  }
  .sub {
    color: #72e3ff;
    margin-left: 50px;
  }
  .navbar {
    margin-top: 140px;      
    background-color: #141414;
    color: #72e3ff;

  }
  .layout{
    background-color: #141414;   
  }
  .header {   
   background-color: #141414;
     
  }  
`

export default HeaderDesk