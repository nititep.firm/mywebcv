import React from 'react'
import Responsive from 'react-responsive';
import 'antd/dist/antd.css';
import { Layout, Icon , Row, Col , Carousel  } from 'antd';

import styled from 'styled-components'

const SlideShow = ({mainPhoto , groupOfMe}) => {
    return    (<Carousel autoplay className="ant-slide">
                <div><img src={mainPhoto} className="main-img"/></div>
                <div><img src={groupOfMe} className="group-img"/></div>
                <div><h3>3</h3></div>
                <div><h3>4</h3></div>
                </Carousel>
        ) 
}

export default SlideShow