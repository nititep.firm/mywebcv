import React from 'react' 

import { Row, Col, Layout } from 'antd'
import styled from 'styled-components';

const { Content } = Layout
const Exp = () => {
    return (<Wrapper>
        <Content className="exp">
        <h1>Experience</h1><hr className="hr-line"/>
        <Row>
        <Col span={10}><p className="exp-text">2014 - Graduated Wat Suthi Wararam School (Math - Sci) </p></Col>
        <Col span={4}>
        <div className="circle"></div>
        <div className="line"></div>
        </Col>
        <Col span={10}></Col>    
        </Row>
        <Row>
        <Col span={10}>  </Col>
        <Col span={4}>
        <div className="circle"></div>
        <div className="line"></div>
        </Col>
        <Col span={10}><p className="exp-text">2015 - Study at Silpakorn University (ICT Advertising Major) </p></Col>    
        </Row>
        <Row>
        <Col span={10}><p className="exp-text">2017 - Marketing Trainee at Man Power Group</p></Col>
        <Col span={4}>
        <div className="circle"></div>
        <div className="line"></div>
        </Col>
        <Col span={10}>  </Col>    
        </Row>
        <Row>
        <Col span={10}>  </Col>
        <Col span={4}>
        <div className="circle"></div>
        <div className="line"></div>
        </Col>
        <Col span={10}> <p className="exp-text">2018 - Winner Hackathon Education Disruption for 200,000 bath</p></Col>    
        </Row>
        <Row>
        <Col span={10}> <p className="exp-text">2018 - Work remotely as Full Stack Developer at Codekit.co</p></Col>
        <Col span={4}>
        <div className="circle"></div>
        <div className="line"></div>
        </Col>
        <Col span={10}></Col>    
        </Row>
        <Row>
        <Col span={10}></Col>
        <Col span={4}>
        <div className="circle"></div>       
        </Col>
        <Col span={10}></Col>    
        </Row>
        </Content> 

    </Wrapper>)
}

const Wrapper = styled.div`

.exp {    
    height: 1400px;
    background-color: #141414;
    h1 {      
      text-align: center;
      color: #72e3ff;
      padding-top: 50px;
    }
    .hr-line{
      width: 200px;
      margin-bottom: 50px;
    }

    .circle{
      width: 30px;
      height: 30px;
      border-radius: 30px;
      background-color: white;
      margin: 0 auto;     
      
    }

    .line { 
      width: 5px;
      height: 200px;;   
      background-color: #ffffff;
      margin: 0 auto;  
    }


.exp-text {
    text-align: center;
    color:  #72e3ff;
    font-size: 20px;
    margin-top: 100px;
}

`

export default Exp