import React from 'react'
import Responsive from 'react-responsive';
import 'antd/dist/antd.css';
import { Menu, Icon } from 'antd';

import styled from 'styled-components'

const MenuBar = () => {
    return <Menu className="navbar"           
    mode="horizontal"
    defaultSelectedKeys={['2']}
    style={{ lineHeight: '64px' }}          >
    <Menu.Item key="1"><Icon type="home" /> Home</Menu.Item>
    <Menu.Item key="2"><Icon type="user" /> About Me</Menu.Item>
    <Menu.Item key="3"><Icon type="edit" /> Skill</Menu.Item>             
    <Menu.Item key="4"><Icon type="star-o" /> Experience</Menu.Item>
    <Menu.Item key="5"><Icon type="star-o" /> Portfolio</Menu.Item>
    <Menu.Item key="6"><Icon type="mail" /> Contact</Menu.Item>
    </Menu>
}

export default MenuBar