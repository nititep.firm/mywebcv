import React from 'react'

const Button = ( { changeName,  subTitle } ) => {
    return (
        <div>        
            <div>
            {subTitle.map( (v,index) => <button key= {v+index} onClick={ () => changeName(v)}>Click {index}</button>)}
            </div>  
        </div>
    )
}

export default Button 