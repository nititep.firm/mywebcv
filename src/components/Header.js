import React from 'react';


import styled from 'styled-components'


const Header = ( { myName , subTitle} ) => {
  return  (<Wrapper >
            <div className="myName"><p>{ myName }</p><p className="sub-title">{ subTitle }</p></div>
            <nav className="nav">
                    <a className="menu">Home</a>
                    <a className="menu">About me</a>
                    <a className="menu">Gallery</a>
                    <a className="menu">Reel</a>
                    <a className="menu">Contact</a>
            </nav>
          </Wrapper>)  
}



const Wrapper = styled.div` 

    body{
    background-color: #141414;
    }

    .myName {
    color: #72e3ff;
    font-size: 40px;
    position: absolute;
    top: 70px;
    left: 150px;
    margin: 0;   
    
  }
  .sub-title {
    font-size: 20px;
    color: #FFFFFF;
    margin: 0 auto;
   }
  .nav {
    color: #72e3ff;
    position: relative;
    top: 150px; 
    right: 100px;   
    font-size: 20px;
    overflow: hidden; 
    text-align: center; 
    float: right;   
  }
  
 .menu {
     margin-left: 20px;
 }

 

  @media screen and (min-width: 600px;){
      
  }
`;

export default Header;