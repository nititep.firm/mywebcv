import React from 'react'
import {Layout} from 'antd'
import styled from 'styled-components'


const { Content } = Layout

const About = ( { profileFb } ) => {
    return (<Wrapper>
        <Content className="aboutme">
        <h1>About Me</h1><hr className="hr-line"/>
        <div className="flex-about">  

                <div className="left">
                    <span className="title">Fullname</span>        
                    <br/><span className="sub" >Nititep Chareonbamrung (Firm)</span>
                    <br/>
                    <span className="title">Birthday</span>
                    <br/>
                    <span className="sub" >22 August 1995</span>
                    <br/>
                    <span className="title">Age</span>
                    <br/>
                    <span className="sub">22 Years 7 Month</span>
                </div>
            
                <div className="center">
                    <img src={profileFb} className="profilefb"/>
                </div>

                <div className="right">
                    <span className="title">Language</span>        
                    <br/><span className="sub">Thai , English</span>
                    <br/>
                    <span className="title">Nationality</span>
                    <br/>
                    <span className="sub">Thai</span>
                    <br/>
                    <span className="title">Stat</span>
                    <br/>
                    <span className="sub">Height 175 cm, Weight 55 kg</span>        
                </div>        
                
        </div>
        </Content>
    </Wrapper>)
}

const Wrapper = styled.div`
.aboutme {    
    
    background-color: #141414;

    h1 {      
      text-align: center;
      color: #72e3ff;
      padding-top: 50px;
    }
    .hr-line{
      width: 200px;
      margin-bottom: 50px;
    }
    .flex-about {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }
    .profilefb{
      width: 200px;
      border-radius: 200px;
      display: block;      
      margin: auto;      
    }
    .left {
      margin-top: 50px;
      margin-right: 30px;      
      .title {
        margin-left: 100px;
        text-align: right;                    
        font-size: 20px;
        color: #72e3ff; 
      }
      .sub {
        color: #ffffff;
        font-size: 15px;        
      }
    }
    .center {
        padding: 50px;
    }
    .right {
      margin-top: 50px;
      margin-left: 30px;     
      .title {
        color: #72e3ff;
        font-size: 20px;        
      }
      .sub {
        color: #ffffff;
        font-size: 15px;
        margin-left: 30px;     
      }
    }   
    
  }


`

export default About